#!/bin/bash
set -euo pipefail

function usage {
    echo "usage: download.sh <parameter_profile_file> <target_dir>"
    exit 1
}

if [ $# -ne 2 ]; then
    usage
fi
PARAMETER_PROFILE_FILE=$1
TARGET_DIR=$2

if [ ! -f $PARAMETER_PROFILE_FILE ]; then
    echo "Parameter profile file not found: $PARAMETER_PROFILE_FILE"
    usage
fi


if [ ! -d $TARGET_DIR ]; then
    echo "Target dir not found: $TARGET_DIR"
    usage
fi

echo "download data to target dir: $TARGET_DIR"

# From https://www.data.gouv.fr/fr/datasets/donnees-carroyees-a-200m-sur-la-population/
DATASET_URL='https://www.data.gouv.fr/fr/datasets/r/6072929c-ba60-4a15-b797-ee3c1b20e21b'
ZIP_FILE_NAME=carroyage_insee_metro.zip

cd $TARGET_DIR
wget $DATASET_URL -O $ZIP_FILE_NAME
unzip $ZIP_FILE_NAME

# Download cycles GeoJSON file
CYCLES_URL=`jq -r '.cycles_url' $PARAMETER_PROFILE_FILE`
wget $CYCLES_URL -O cycles.geojson

echo "done."