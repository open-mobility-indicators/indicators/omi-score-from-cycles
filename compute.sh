#!/bin/bash
set -euo pipefail

function usage {
    echo "usage: compute.sh <source_dir> <target_dir>"
    exit 1
}

if [ $# -ne 2 ]; then
    usage
fi
SOURCE_DIR=$1
TARGET_DIR=$2

if [ ! -d $SOURCE_DIR ]; then
    echo "Source dir not found: $SOURCE_DIR"
    usage
fi

if [ ! -d $TARGET_DIR ]; then
    echo "Target dir not found: $TARGET_DIR"
    usage
fi

echo "compute data from source dir: $SOURCE_DIR to target dir: $TARGET_DIR"

# Temp directory
TEMP_DIR=`mktemp -d`
echo "Temporary directory $TEMP_DIR created"

# Run notebook
papermill \
   -p SOURCE_DIR $SOURCE_DIR \
   -p TARGET_DIR $TARGET_DIR \
   notebook.ipynb $TEMP_DIR/output.ipynb

if [ -d $TEMP_DIR ]; then
    echo "cleaning temp dir..."
    rm -fR $TEMP_DIR
fi

echo "done."